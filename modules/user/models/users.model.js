'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');
var _ = require('lodash');

var Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  password: {
    type: String,
    default: ""
  }
});

module.exports = mongoose.model("Users", UserSchema);