'use strict';

/**
 * Module dependencies.
 */
const mongoose = require('mongoose');
const path = require('path');
const _ = require('lodash');
const autoIncrement = require('mongoose-auto-increment');

const Schema = mongoose.Schema;

const FoodSchema = new Schema({
  id: {type: Number, required: true, unique: true},
  shop: {type: String, default: ''},
  name: {type: String, default: ''},
  type: {type: String, default: ""},
  image: {type: String, default: ""},
  image_extends: [String],
  created_at: {type: Date, default: null},
  updated_at: {type: Date, default: null},
  describe: {type: String, default: ""},
  price:  {type: Number, default: 0},
});
autoIncrement.initialize(mongoose.connection);
FoodSchema.plugin(autoIncrement.plugin,{
  model: 'Foods',
  field: 'id',
  startAt: 100000,
  incrementBy: 1
});

module.exports = mongoose.model("Foods", FoodSchema);